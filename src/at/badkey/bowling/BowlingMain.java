/**
 * 
 */
package at.badkey.bowling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BowlingMain {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			
			try {
				System.out.write("Enter input: ".getBytes());
				
				String input = reader.readLine();
				
				if(input.length() == 0) continue;
				 
				if(input.equalsIgnoreCase("q") || input.equalsIgnoreCase("quit") || input.equalsIgnoreCase("e") || input.equalsIgnoreCase("exit")) break;
				
				System.out.println(new InputParser(input).generateOutput());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
