/**
 * 
 */
package at.badkey.bowling;

public class InputParser {

	private String input;
	
	protected InputParser(String input) {
		this.input = input;
	}
	
	public String generateOutput() {
		String[] rounds = input.split(":")[1].split(",");
		int iR = Integer.parseInt(input.split(":")[0]);
		BowlingGame game = new BowlingGame();
		for(String s : rounds) game.addThrow(Integer.parseInt(s));
		
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0;i<iR;i++) {
			if(i != 0) sb.append(",");
			sb.append(game.getScore(i));
		}
		
		return sb.toString();
	}
	
}
