/**
 * 
 */
package at.badkey.bowling;

import java.util.ArrayList;

public class BowlingGame {
	
	private ArrayList<Round> rounds = new ArrayList<Round>();
	
	private int[] bonusCycle = new int[] {0,2,1};
	private boolean bonusCycleEnabled = false;
	
	protected BowlingGame() {
		
	}
	
	public ArrayList<Round> getRounds() {
		return rounds;
	}
	
	public void addThrow(int pins) {
		if(rounds.size() == 0 || rounds.get(rounds.size() - 1).isComplete()) {
			rounds.add(new Round(this));
			addThrow(pins);
			return;
		}
		Round r = rounds.get(rounds.size() - 1);
		if(bonusCycleEnabled) {
			int index = rounds.indexOf(r);
			r.bonus = bonusCycle[index % bonusCycle.length];
		}
		r.addThrow(pins);
	}
	
	public int getScore(int round) {
		int score = 0;
		for(int i = 0;i<=round;i++) {
			Round r = rounds.get(i);
			score+= r.getBonus() + r.getScore();
		}
		return score;
	}
	
	public int getScore() {
		int score = 0;
		for(Round r : rounds) score+= r.getBonus() + r.getScore();
		return score;
	}
	
}
