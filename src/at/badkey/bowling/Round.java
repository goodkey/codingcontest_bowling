/**
 * 
 */
package at.badkey.bowling;

import java.util.ArrayList;

public class Round {

	private boolean complete = false;
	private BowlingGame instance;
	
	private final int MAX_THROWS = 2;
	private final int MAX_PINS = 10;
	
	public int bonus = 0;
	
	private ArrayList<Integer> pins = new ArrayList<Integer>();
	
	protected Round(BowlingGame game) {
		instance = game;
	}
	
	public boolean isComplete() {
		return complete;
	}
	
	public ArrayList<Integer> getPins() {
		return pins;
	}
	
	public int getScore() {
		int score = 0;
		for(int i : pins) score += i;
		return score;
	}
	
	public int getBonus() {
		if(bonus == 0) return 0;
		
		int add = 0;
		int toAdd = bonus;
		
		ArrayList<Round> rounds = instance.getRounds();
		int index = rounds.indexOf(this) + 1;
		
		while(toAdd > 0) {
			if(rounds.size() == index) break;
			
			Round r = rounds.get(index);
			
			for(int i = 0;i<r.getPins().size();i++) {
				if(toAdd > 0) {
					add += r.getPins().get(i);
					toAdd--;
				}else break;
			}
			
			index ++;
		}
		
		return add;
	}
	
	public void addThrow(int pins) {
		if(complete) return;
		
		this.pins.add(pins);
		if(this.pins.size() == MAX_THROWS || getScore() == MAX_PINS) {
			complete = true;
			if(pins == MAX_PINS) {
				bonus = 2; //STRIKE
			}else if(getScore() == MAX_PINS) bonus = 1; //SPARE
		}
	}
	
}
