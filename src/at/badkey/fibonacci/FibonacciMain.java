/**
 * 
 */
package at.badkey.fibonacci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FibonacciMain {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			
			try {
				System.out.write("Enter input: ".getBytes());
				
				String input = reader.readLine();
				
				if(input.length() == 0) continue;
				 
				if(input.equalsIgnoreCase("q") || input.equalsIgnoreCase("quit") || input.equalsIgnoreCase("e") || input.equalsIgnoreCase("exit")) break;
				
				System.out.println(calcResult(Integer.parseInt(input)) + "");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public static int calcResult(int x) {
		if(x == 2) return 1;	
		if(x == 3) return 2;	
		return calcResult(x - 1) + calcResult(x - 2);
	}
	
}
